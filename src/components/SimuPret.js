import React, { useState } from 'react'
import Grid from '@material-ui/core/Grid'
import Typography from '@material-ui/core/Typography'
import Slider from '@material-ui/core/Slider'

const SimuPret = () => {
  const [apport, setApport] = useState(30000)
  const [prixTerrain, setPrixTerrain] = useState(130000)
  const [prixMaison, setPrixMaison] = useState(140000)

  const taux = 1.4
  const budgetGlobal = Number(prixTerrain + prixMaison)
  const fraisCourtier = 2000
  const fraisNotaire = Number(prixTerrain * 0.08)
  const fraisGarantie = Number((budgetGlobal / 90).toFixed(2))
  const totalDepense = (
    budgetGlobal +
    fraisNotaire +
    fraisGarantie +
    fraisCourtier
  ).toFixed(2)
  const mensualite = (
    ((totalDepense - apport) * (taux / 10 + 1) + apport) /
    300
  ).toFixed(2)

  return (
    <div>
      <Grid container spacing={2}>
        <Grid item xs={12}>
          <h2>Simulation prêt</h2>
        </Grid>
        <Grid item xs={12}>
          <Grid container>
            <Grid item xs={8}>
              <Typography id='discrete-slider'>Apport perso</Typography>
              <Slider
                value={apport}
                aria-labelledby='discrete-slider'
                step={5000}
                marks
                min={0}
                max={50000}
                valueLabelDisplay='off'
                onChange={(e, newValue) => setApport(newValue)}
              />
            </Grid>
            <Grid item xs={4} className='label-slider'>
              {apport} €
            </Grid>
          </Grid>
        </Grid>
        <Grid item xs={12}>
          <Grid container>
            <Grid item xs={8}>
              <Typography id='discrete-slider'>Prix terrain</Typography>
              <Slider
                value={prixTerrain}
                aria-labelledby='discrete-slider'
                step={5000}
                marks
                min={80000}
                max={170000}
                valueLabelDisplay='off'
                onChange={(e, newValue) => setPrixTerrain(newValue)}
              />
            </Grid>
            <Grid item xs={4} className='label-slider'>
              {prixTerrain} €
            </Grid>
          </Grid>
        </Grid>
        <Grid item xs={12}>
          <Grid container>
            <Grid item xs={8}>
              <Typography id='discrete-slider'>Prix maison</Typography>
              <Slider
                value={prixMaison}
                aria-labelledby='discrete-slider'
                step={5000}
                marks
                min={80000}
                max={170000}
                valueLabelDisplay='off'
                onChange={(e, newValue) => setPrixMaison(newValue)}
              />
            </Grid>
            <Grid item xs={4} className='label-slider'>
              {prixMaison} €
            </Grid>
          </Grid>
        </Grid>
        <Grid item xs={12} className='text-center infos-simu'>
          <Typography align='center' color='primary'>
            Budget global : {budgetGlobal} €
          </Typography>
          <Grid container>
            <Grid item xs={6}>
              Frais de notaire* : <br />
              <b>{fraisNotaire} €</b>
            </Grid>
            <Grid item xs={6}>
              Frais de garantie* : <br />
              <b>{fraisGarantie} €</b>
            </Grid>
            <Grid item xs={6}>
              Frais courtier : <br />
              <b>{fraisCourtier} €</b>
            </Grid>
            <Grid item xs={6}>
              Taux : <br />
              <b>{taux} %</b>
            </Grid>
            <Grid item xs={6}>
              <Typography align='center' color='secondary' variant='h6'>
                Total dépense* : <br />
                <b>{totalDepense} €</b>
              </Typography>
            </Grid>
            <Grid item xs={6}>
              <Typography align='center' color='secondary' variant='h6'>
                Mensualité* : <br />
                <b>{mensualite} €</b>
              </Typography>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </div>
  )
}

export default SimuPret

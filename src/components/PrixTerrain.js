import React, { useState } from 'react'
import TextField from '@material-ui/core/TextField'
import Grid from '@material-ui/core/Grid'
import Chip from '@material-ui/core/Chip'

const PrixTerrain = () => {
  const [prixTerrain, setPrixTerrain] = useState(0)
  const [dimension, setDimensions] = useState(0)

  const RenderPrixM2 = function () {
    const prix = (prixTerrain / dimension).toFixed(0)
    if (isFinite(prix)) {
      return <Chip label={'Prix au m² : ' + prix + '€'} />
    } else {
      return null
    }
  }

  const RenderFraisDeNotaire = function () {
    const frais = (prixTerrain * 0.08).toFixed(0)
    if (frais !== '0') {
      return <Chip label={'Frais de notaire : ' + frais + '€'} />
    } else {
      return null
    }
  }

  return (
    <div>
      <Grid container spacing={2}>
        <Grid item xs={12}>
          <h2>Prix terrain au m²</h2>
        </Grid>
        <Grid item xs={6}>
          <TextField
            id='outlined-basic'
            variant='outlined'
            label='Prix du terrain'
            onChange={(e) => setPrixTerrain(e.target.value)}
          />
        </Grid>
        <Grid item xs={6}>
          <TextField
            id='outlined-basic'
            variant='outlined'
            label='M²'
            onChange={(e) => setDimensions(e.target.value)}
          />
        </Grid>
        <Grid item xs={6}>
          <RenderFraisDeNotaire />
        </Grid>
        <Grid item xs={6}>
          <RenderPrixM2 />
        </Grid>
      </Grid>
    </div>
  )
}

export default PrixTerrain

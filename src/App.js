import React from 'react'
import './App.css'
import PrixTerrain from './components/PrixTerrain'
import SimuPret from './components/SimuPret'

function App() {
  return (
    <div>
      <PrixTerrain />
      <hr />
      <SimuPret />
    </div>
  )
}

export default App
